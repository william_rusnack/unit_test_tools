import inspect

__author__ = 'williamrusnack'


class UnitTestReport:
    # Note: if real_time_report is True log_result prints the outcome of the test when it is called else only the errors
    #           are reported when report is called
    #       change_a_lot is the key that the message input of log_result is stored to
    def __init__(self, real_time_report=False, changing_a_lot='test_note'):
        self.components = {}  # stores the data that that is to be logged
        self.error_log = []  # stores all the log results when the input to log_result is false
        self.real_time_report = real_time_report  # if true log_result will print the test result when it is called
        self.untested_class_functions = []  # stores all the functions that do not have not been tested from a class

        # holds the dictionary key that the message input to log_result will be stored to
        self.changing_a_lot = changing_a_lot

        # holds the dictionary for multi_check_in and multi_check_out
        self.multi_values = {}

    # Requires: boolean_test_result must be able to evaluate to True or False
    #           message must be None, string, or tuple/list of strings
    # Modifies: self.components, self.untested_class_function
    # Effects:  if boolean_test_result is false an error log is created in self.error_log
    #           if message is not none message is logged with the report along with the rest
    #               of the keys and values in self.components
    #           if message is or contains a key that is in self.multi_values the key and
    #               value is removed from self.multi_values
    def log_result(self, boolean_test_result, message=None):
        boolean_test_result = bool(boolean_test_result)
        assert isinstance(boolean_test_result, bool)

        if isinstance(message, str) or message is None:

            # adds message to components (is removed at the bottom of this function)
            if message:
                self.components[self.changing_a_lot] = message

            if boolean_test_result:
                complete_string = 'Success '
            else:
                complete_string = 'Error '

            for key in self.components.keys():
                complete_string += str(key) + ': ' + str(self.components[key]) + ', '
            complete_string = complete_string[0:-2]

            if self.real_time_report:
                print(complete_string)
            if not boolean_test_result:
                self.error_log.append(complete_string)

            try:
                if {'test_class': self.components['test_class'], 'test_function': self.components['test_function']} in \
                        self.untested_class_functions:
                    self.untested_class_functions.pop(self.untested_class_functions.index(
                        {'test_class': self.components['test_class'], 'test_function': self.components['test_function']})
                    )
            except KeyError:
                None

            # clears message from self.components
            if message:
                del self.components[self.changing_a_lot]

        # for multiple in log messages with the same result
        elif isinstance(message, list) or isinstance(message, tuple):
            for mes in message:
                self.log_result(boolean_test_result, mes)
        else:
            assert False

    # Requires: test_class needs to be a class handle
    # Effects:  adds the class method names to self.untested_class_functions
    def add_class(self, test_class):
        self.components['test_class'] = test_class.__name__
        self.untested_class_functions += [{'test_class': test_class.__name__, 'test_function': function[0]} for function in
                                          inspect.getmembers(test_class, predicate=inspect.isfunction)]

    # Requires: key_and_values to be a dictionary.
    #           the keys in key_and_values need to be a category inputted to changing_a_lot when the self was declared
    #           the objects that the keys in key_and_values point to must be able to evaluate to True or False
    # Modifies: self.multi_values
    # Effects:  if there are keys in key_and_values that have not been inputted yet the key and its value is added to
    #               self.multi_values
    #           if the key has already been added the value of the key is combined with it's previous value with the
    #               and operator
    # Note:     useful for testing multiple cases
    #           keys can be inputted multiple times but once false will stay false
    #           to log the results inputted call the multi_check_out function
    def multi_check_in(self, check_in_keys, common_value=None):
        if common_value is not None:
            common_value = bool(common_value)
        if isinstance(check_in_keys, str) and \
                common_value is not None and \
                (bool(common_value) == False or bool(common_value) == True):
            check_in_keys = [check_in_keys]
        if (isinstance(check_in_keys, list) or isinstance(check_in_keys, tuple)) and \
                common_value is not None and \
                (bool(common_value) == False or bool(common_value) == True):
            change_to_dictionary = {}
            for key in check_in_keys:
                change_to_dictionary[key] = common_value
            check_in_keys = change_to_dictionary
        assert isinstance(check_in_keys, dict)
        for key in check_in_keys.keys():
            try:
                self.multi_values[key] &= check_in_keys[key]
            except KeyError:
                self.multi_values[key] = bool(check_in_keys[key])

    # Modifies: self.multi_values
    # Effects:  logs all the results that were inputted to multi_check_in
    #           empties the keys that were inputted with multi_check_in
    def multi_check_out(self):
        for key in self.multi_values.keys():
            self.log_result(self.multi_values[key], key)
        self.multi_values = {}

    # Modifies: self.multi_values
    # Effects:  calls multi_check_out
    #           prints all the untested class functions
    #           prints all the logs that evaluated to false
    def report(self):
        self.multi_check_out()
        if len(self.untested_class_functions) > 0:
            print()
            print('UNTESTED FUNCTIONS')
            print(self.untested_class_functions)
        if len(self.error_log) > 0:
            print()
            print('ERRORS FOUND')
            for error in self.error_log:
                print(error)
        if len(self.untested_class_functions) + len(self.error_log) == 0:
            print()
            print('TOTAL SUCCESS GOOD JOB')
            return True
        else:
            return False

    def __del__(self):
        self.report()


# not related to above class and should be relocated
